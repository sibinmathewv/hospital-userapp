import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
    private geolocation: Geolocation) {

  }
  call()
  {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log("location"+resp.coords.latitude,resp.coords.longitude)
     
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }
}
